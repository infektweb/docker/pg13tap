FROM postgres:13

RUN apt-get update
RUN apt-get install -y git make patch postgresql-13-cron
RUN git -C /tmp clone --depth 1 https://github.com/theory/pgtap
RUN cd /tmp/pgtap && make && make install
COPY pgtap.sql /docker-entrypoint-initdb.d/pgtap.sql

COPY postgres.conf /etc/postgresql/postgresql.conf